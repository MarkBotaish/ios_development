//
//  ChangeDateViewController.swift
//  Homepwner
//
//  Created by user166622 on 3/25/20.
//  Copyright © 2020 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ChangeDateViewController: UIViewController {

    var item: Item!

    @IBAction func ChangeDate(_ sender: UIDatePicker) {
        item.dateCreated = sender.date;
    }
    
}
