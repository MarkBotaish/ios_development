//
//  CustomUITextField.swift
//  Homepwner
//
//  Created by user166622 on 3/25/20.
//  Copyright © 2020 Big Nerd Ranch. All rights reserved.
//

import UIKit

class CustomUITextField: UITextField {

    override func becomeFirstResponder() -> Bool {
        self.borderStyle = UITextField.BorderStyle.line
        return super.becomeFirstResponder()
     }
    
    override func resignFirstResponder() -> Bool {
        self.borderStyle = UITextField.BorderStyle.roundedRect
        return super.resignFirstResponder()
    }

}
