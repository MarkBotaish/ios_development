//
//  NotesViewController.swift
//  Project2
//
//  Created by user166622 on 4/1/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit

class NotesViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    var note : NotesModel!
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var noteTitle: UITextField!
    @IBOutlet weak var noteImage: UIButton!
    @IBOutlet weak var noteDetails: UITextView!
    @IBOutlet weak var favoriteButton: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set a border for the text field to show where to click and make more arty :)
        noteDetails.layer.borderColor = UIColor.lightGray.cgColor
        noteDetails.layer.borderWidth = 1
        
        //Init the view controller
        noteTitle.text = note.title
        noteDetails.text = note.notes
        if(note.image != nil){ //If the image exists set it
            noteImage.setImage(note.image, for: .normal)
        }
        
        //Toggle favorite button
        SetFavoriteButton(note.isFavorite)
    }
    
    //Toggles the filling in of the star
    func SetFavoriteButton(_ isFavorite : Bool){
        if(isFavorite){favoriteButton.image = UIImage(systemName: "star.fill")}
        else{favoriteButton.image = UIImage(systemName: "star")}
    }
    
    //Save data to the cell
    override func viewWillDisappear(_ animated: Bool) {
            note.title = noteTitle.text!
            note.notes = noteDetails.text!
            note.image = noteImage.imageView?.image            
    }
    
    //Favorite the cell
    @IBAction func FavoriteNote(_ sender: UIBarButtonItem) {
        note.isFavorite = !note.isFavorite
         SetFavoriteButton(note.isFavorite)
        
        //Add to the favorites list
        if(note.isFavorite){ Favorite.shared.add(note) }
        else {Favorite.shared.remove(note)}
    }
    
    //Chance the image of the notee
    @IBAction func changeImage(_ sender: UIButton) {
        //Open saved photos
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false

            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //Choose the photo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            noteImage.setImage(image, for: .normal) //set the photo after choosing
        }
        
        dismiss(animated: true, completion: nil)
    }
    
   

}
