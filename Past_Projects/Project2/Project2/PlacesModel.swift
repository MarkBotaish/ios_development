//
//  PlacesModel.swift
//  Project2
//
//  Created by user166622 on 4/1/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit

class PlacesModel: FavoriteCell {
    
    var name : String
    var details : String
    var imageName : String
    var image : UIImage!
    var latitiude : Double = 0
    var longitude : Double = 0
    
    init(_ name : String, _ image : String, _ details : String, _ latitiude : Double, _ longitude : Double,
         _ isFavorite : Bool) {
        self.details = details
        self.name = name
        self.longitude = longitude
        self.latitiude = latitiude
        self.imageName = image
        
        if let asset = UIImage(named: image){self.image = asset}
        else {print("Could not load image \(image)")}
        
        super.init(Type.PLACES)
        self.isFavorite = isFavorite
    }
    
    //Loading in Saved Data
    required convenience init?(coder: NSCoder) {
        guard let saveName = coder.decodeObject(forKey: "name") as? String,
            let savedetails = coder.decodeObject(forKey: "details") as? String,
            let saveImage = coder.decodeObject(forKey: "image") as? String,
            let saveLat = coder.decodeDouble(forKey: "lat") as? Double,
            let saveLong = coder.decodeDouble(forKey: "long") as? Double
            else{print("Encoding failled"); return nil}
        self.init  (saveName, saveImage, savedetails, saveLat, saveLong, coder.decodeBool(forKey: "fav"))
    }
    
    //Encoding Saved Data
    override func encode(with coder: NSCoder) {
        coder.encode(name, forKey: "name")
        coder.encode(details, forKey: "details")
        coder.encode(imageName, forKey: "image")
        coder.encode(latitiude, forKey: "lat")
        coder.encode(longitude, forKey: "long")
        coder.encode(isFavorite,forKey: "fav")
    }
    
    //Compare two PlacesModel. Returns true if they are the same
    func compare (other : PlacesModel) -> Bool{
        if(other.name != self.name) {return false}
        if(other.details != self.details) {return false}
        if(other.imageName  != self.imageName) {return false}
        if(other.latitiude  != self.latitiude   ) {return false}
        if(other.longitude  != self.longitude) {return false}
        return true
    }
    
}
