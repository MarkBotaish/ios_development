//
//  FavoriteTableViewController.swift
//  Project2
//
//  Created by user166622 on 4/1/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit

class FavoriteTableViewController: UITableViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
       navigationItem.leftBarButtonItem = editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    deinit {
        Favorite.shared.Save() //Save all dataa when the app closes
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //Set the height of the cell based on the type of cell
        let favCell = Favorite.shared.favoriteCells[indexPath.row]
        if(favCell.type == Type.PLACES){return 100}
        return 50
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Favorite.shared.favoriteCells.count;
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Data", for: indexPath)
        
        //Init the cell based on the type of the cell
        let favCell = Favorite.shared.favoriteCells[indexPath.row]
        if(favCell.type == Type.NOTES){
            let note = favCell as! NotesModel
            cell.textLabel?.text = note.title
            cell.imageView?.image = note.image
        }else if(favCell.type == Type.PLACES){
            let place = favCell as! PlacesModel
            cell.textLabel?.text = place.name
            cell.imageView?.image = place.image
            
        }else{
            print("Type Not Set")
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let favCell = Favorite.shared.favoriteCells[indexPath.row]
        var viewController : UIViewController?
        
        //Perform "segues" based on the type of cell
        if(favCell.type == Type.NOTES){
             let note = favCell as! NotesModel
            viewController = storyboard?.instantiateViewController(withIdentifier: "NotesID")
            if let notecontroller = viewController as? NotesViewController{
                notecontroller.note = note
            }
        }else if(favCell.type == Type.PLACES){
             let place = favCell as! PlacesModel
            viewController = storyboard?.instantiateViewController(withIdentifier: "PlacesID")
            if let placeController = viewController as? PlacesViewController{
                placeController.place = place
            }
        }else{
            print("Type Not Set")
        }
        self.navigationController?.pushViewController(viewController!, animated: true)
    }

   override func tableView(_ tableView: UITableView,
        moveRowAt sourceIndexPath: IndexPath,
        to destinationIndexPath: IndexPath) {
            // Update the model
        Favorite.shared.moveNoteTo(start: sourceIndexPath.row, to: destinationIndexPath.row)
    }

    override func tableView(_ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath) {
            // If the table view is asking to commit a delete command...
            if editingStyle == .delete {
                let cell = Favorite.shared.favoriteCells[indexPath.row]
                cell.isFavorite = false
                Favorite.shared.remove(cell)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                Favorite.shared.remove(cell)
            }
    }
    

}
