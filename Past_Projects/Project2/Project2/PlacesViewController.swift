//
//  PlacesViewController.swift
//  Project2
//
//  Created by user166622 on 4/1/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit
import MapKit

class PlacesViewController: UIViewController {

    var place : PlacesModel!
    

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var details: UITextView!
    @IBOutlet weak var favoriteButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Init the view controller
        image.image = place.image
        details.text = place.details
        SetFavoriteButton(place.isFavorite)
        SetUpMap()
        
    }

    //Favorite the places
    @IBAction func FavoritePlace(_ sender: UIBarButtonItem) {
        place.isFavorite = !place.isFavorite
        SetFavoriteButton(place.isFavorite)
        if(place.isFavorite){ Favorite.shared.add(place) }
        else {Favorite.shared.remove(place)}
    }
    
    //Opens Apple maps
    @IBAction func GetDirections(_ sender: UIButton) {
        
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: place.latitiude, longitude: place.longitude)))
        
        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]

        mapItem.openInMaps(launchOptions: launchOptions)
 
    }
    
    //Set up the map for the view controller
    func SetUpMap(){
        //Placemarker
        let location = CLLocationCoordinate2D(latitude: place.latitiude,
                                              longitude: place.longitude)
        
        //View size
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        map.setRegion(region, animated: true)
        
        //Visuals
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = place.name
        map.addAnnotation(annotation)
    }
    
    //Toggle favorite button
    func SetFavoriteButton(_ isFavorite : Bool){
        if(isFavorite){favoriteButton.image = UIImage(systemName: "star.fill")}
        else{favoriteButton.image = UIImage(systemName: "star")}
    }
    
    
}
