//
//  PlacesTableViewController.swift
//  Project2
//
//  Created by user166622 on 4/1/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//


import UIKit
import Foundation

class PlacesTableViewController: UITableViewController {

    
    var places : [PlacesModel] = []
    var currentIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Load all the favorite cells.
        //This is always the first function called when the program starts
        Favorite.shared.Load()
        
        //Read in the places data from a plist
        if let path = Bundle.main.path(forResource: "Places", ofType: "plist"){
            let dict = (NSDictionary(contentsOfFile: path) as? Dictionary<String, [String]>)!
            let index = Array(dict.keys)
            
            for i in index{
                let array = dict[i]
                var thePlace = PlacesModel(String(i), (array?[0])!, (array?[1])!, Double((array?[2])!)!,
                                           Double((array?[3])!)!, false)
                
                //If the cell is already in the favorites list, add it to this list and set it to be favorited
                if let cell = Favorite.shared.Compare(thePlace) {
                    places.append(cell)
                    cell.isFavorite = true
                }else{
                    places.append(thePlace)
                }
            }
            
        }else{
            print("Could not load file!")
        }
        
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return places.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Places", for: indexPath)
        
        //Init the cell
        cell.textLabel?.text = places[indexPath.row].name
        cell.imageView?.image = places[indexPath.row].image
        // Configure the cell...
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        performSegue(withIdentifier: "Places", sender: self)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let controller = segue.destination as? PlacesViewController{
            controller.place = places[currentIndex]
        }
    }

}
