//
//  FavoriteCell.swift
//  Project2
//
//  Created by user166622 on 4/1/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit

//Types of cells
enum Type{
    case DEFAULT
    case NOTES
    case PLACES
}

class FavoriteCell : NSObject, NSCoding {

    var type: Type = Type.DEFAULT
    var isFavorite : Bool = false;
    
    init(_ type: Type){
        self.type = type
    }
    
    //Overrided in subclass
    func encode(with coder: NSCoder) {
          
    }
    
    //Overrided in subclass
    required init?(coder: NSCoder) {
       
    }
   

}
