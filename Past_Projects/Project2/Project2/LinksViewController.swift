//
//  LinksViewController.swift
//  Project2
//
//  Created by user166622 on 3/30/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit

class LinksViewController: UITableViewController {

    
    var titles : [String] = []
    var links : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Load information of links from a plist
        if let path = Bundle.main.path(forResource: "Links", ofType: "plist"){
            let dict = (NSDictionary(contentsOfFile: path) as? Dictionary<String, String>)!
            titles = Array(dict.keys)
            links = Array(dict.values)
        }else{
            print("Could not load file!")
        }
    }
    

    override func numberOfSections(in tableView: UITableView) -> Int {
       // #warning Incomplete implementation, return the number of sections
       return 1
   }

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // #warning Incomplete implementation, return the number of rows
        return links.count;
   }
   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Link", for: indexPath)
        cell.textLabel!.text = titles[indexPath.row]
        return cell
   }
   
   override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 50.0
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //open link
        guard let url = URL(string: links[indexPath.row])else{print("Coud not find URL"); return;}
        UIApplication.shared.open(url)
   }
}
