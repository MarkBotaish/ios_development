//
//  NotesModel.swift
//  Project2
//
//  Created by user166622 on 4/1/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit

class NotesModel: FavoriteCell {
    
    var title : String = ""
    var notes : String = ""
    var image : UIImage!
    
    init(title : String, notes : String, image : UIImage!, isFavorite : Bool) {
        super.init(Type.NOTES)
        self.title = title;
        self.notes = notes;
        self.image = image;
        self.isFavorite = isFavorite
    }
    
    //Decodes the data
    required convenience init?(coder: NSCoder) {
        guard let saveTitle = coder.decodeObject(forKey: "title") as? String,
            let saveNotes = coder.decodeObject(forKey: "note") as? String,
            let saveFavorite : Bool = coder.decodeBool(forKey: "fav")
            else{print("Enconding failled"); return nil}
        self.init(title: saveTitle, notes: saveNotes, image: coder.decodeObject(forKey: "image") as? UIImage, isFavorite: saveFavorite )
    }
    
    //Encodes the data
    override func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "title")
        coder.encode(notes, forKey: "note")
        coder.encode(image, forKey: "image")
        coder.encode(isFavorite, forKey: "fav")
    }
    
    //compate note to another note. Returns true if they are the same
    func compare (other : NotesModel) -> Bool{
        if(other.title != self.title) {print("TITLE");return false}
        if(other.notes != self.notes) {print("TITLE1");return false}
        if(other.isFavorite != self.isFavorite) {print("TITLE3");return false}
        return true
    }
}
