//
//  NotesTableViewController.swift
//  Project2
//
//  Created by user166622 on 3/31/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit

class NotesTableViewController: UITableViewController {

    
    var book : Book = Book()
    var currentNote : NotesModel? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        book.Load() //Load all notes from the save file
        navigationItem.leftBarButtonItem = editButtonItem
    }
    
    //When closing the app save the data of the notes
    deinit {
        book.Save()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return book.notes.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Notes", for: indexPath)
        
        //Init the cell
        cell.textLabel?.text = book.notes[indexPath.row].title
        cell.imageView?.image = book.notes[indexPath.row].image

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            currentNote = book.notes[indexPath.row]
            performSegue(withIdentifier: "ViewNote", sender: self)
     
     }
    
    override func tableView(_ tableView: UITableView,
        moveRowAt sourceIndexPath: IndexPath,
        to destinationIndexPath: IndexPath) {
            // Update the model
        book.moveNoteTo(start: sourceIndexPath.row, to: destinationIndexPath.row)
    }
    
    override func tableView(_ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath) {
            // If the table view is asking to commit a delete command...
            if editingStyle == .delete {
                let theNote = book.notes[indexPath.row]
                self.book.removeNote(note: theNote)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                Favorite.shared.remove(theNote) //Reorder the notes
            }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let controller = segue.destination as? NotesViewController{
            controller.note = currentNote
        }
    }
    
    //Add a new note to the book and the table
    @IBAction func AddNewNote(_ sender: Any) {
        let theNote: NotesModel = NotesModel(title: "Untitled", notes: "Nothing to see here...", image: nil, isFavorite: false)
        book.addNote(note: theNote)
        let indexPath = IndexPath(row: book.notes.count - 1, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
}
