//
//  Favorite.swift
//  Project2
//
//  Created by user166622 on 4/1/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit

class Favorite: UIResponder, UIApplicationDelegate{
    
    static let shared = Favorite()
    var favoriteCells : [FavoriteCell] = []
    
    func Load(){
       do{
            let fileManager = FileManager()
            let directory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            var saveFile = directory.appendingPathComponent("favorite.plist")
            
            let ReadData = try NSData(contentsOf: saveFile, options: .dataReadingMapped)
            let savedCells = try NSKeyedUnarchiver.unarchiveObject(with: ReadData as Data)
            if(savedCells == nil) {return}
            for cell in savedCells as! [FavoriteCell]{
                add(cell)
            }
        
            saveFile.removeAllCachedResourceValues()
            
        }catch{
            //print(error)
        }
    }
    
    func Save() {
         do{
            let fileManager = FileManager()
            let directory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let saveFile = directory.appendingPathComponent("favorite.plist")
                   
            let libraryData = try NSKeyedArchiver.archivedData(withRootObject: favoriteCells, requiringSecureCoding: false)
            try libraryData.write(to: saveFile)
            print(libraryData)
                         
        }catch{
            print(error)
        }
    }
    
    
    func add(_ cell : FavoriteCell){
        favoriteCells.append(cell)
    }
    
    func remove(_ cell: FavoriteCell){
        if let index = favoriteCells.firstIndex(of : cell){
            favoriteCells.remove(at: index)
        }
    }
    
    func moveNoteTo(start : Int, to : Int){
        let theCell = favoriteCells[start]
                
        favoriteCells.remove(at: start)
        favoriteCells.insert(theCell, at: to)
    }
    
    //Compare Favorite cell to NotesModel. Returns NotesModel if found to be the same
    func Compare(_ note : NotesModel) -> NotesModel?{
        for cell in favoriteCells{
            if let noteMod = cell as? NotesModel {               
                if(note.compare(other: noteMod)) {return noteMod}
            }
        }
        return nil
    }
    
    //Compare Favorite cell to PlacesModel. REsturns PlacesModel if found to be the same
    func Compare(_ place : PlacesModel) -> PlacesModel?{
        for cell in favoriteCells{
            if let placeMod = cell as? PlacesModel {
                if(place.compare(other: placeMod)) {return placeMod}
            }
        }
        return nil
    }
    
    
    
}
