//
//  Book.swift
//  Project2
//
//  Created by user166622 on 4/1/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import Foundation

class Book {
    
    var notes: [NotesModel] = []
    
    //Get a pList from the disk and loads the save data
    func Load() {
       do{
            let fileManager = FileManager()
            let directory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            var saveFile = directory.appendingPathComponent("objects.plist")
            
            let ReadData = try NSData(contentsOf: saveFile, options: .dataReadingMapped)
            let savedNotes = try NSKeyedUnarchiver.unarchiveObject(with: ReadData as Data)
            if(savedNotes == nil) {return}

            for note in savedNotes as! [NotesModel]{
               //If the two cells are the same add it from the favorites list
                if let cell = Favorite.shared.Compare(note){
                    notes.append(cell)
                }else{ //Add create a new cell
                    addNote(note: note)
                }
               
            }
            //Clear any cache
            saveFile.removeAllCachedResourceValues()
            
        }catch{
            //print(error)
        }
    }
    
    //Load the data into a plist save file
    func Save(){
         do{
            let fileManager = FileManager()
            let directory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let saveFile = directory.appendingPathComponent("objects.plist")
                   
            let libraryData = try NSKeyedArchiver.archivedData(withRootObject: notes, requiringSecureCoding: false)
            try libraryData.write(to: saveFile)
                         
        }catch{
            print(error)
        }
    }
    
    func addNote(note : NotesModel)  {
        notes.append(note)
    }
    
    func removeNote(note : NotesModel){
        if let index = notes.firstIndex(of : note){
            notes.remove(at: index)
        }
    }
    
    func moveNoteTo(start : Int, to : Int){
        let theNote = notes[start]
                
        notes.remove(at: start)
        notes.insert(theNote, at: to)
    }
    
}
