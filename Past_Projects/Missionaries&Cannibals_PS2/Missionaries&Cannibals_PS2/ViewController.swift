//
//  ViewController.swift
//  Missionaries&Cannibals_PS2
//
//  Created by Botaish, Mark on 1/31/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var numOfCrossings : Int = 0;
    
    var missionaries : [UIButton] = []
    var cannibals : [UIButton] = []
    var selectedButtons :  [UIButton] = []
    
    var numOfMissionOnLeft = 3
    var numOfCannOnLeft = 3
    
    @IBOutlet weak var crossingLabel: UILabel!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var resetBtn: UIButton!
    
    @IBOutlet weak var rightBoat: UIImageView!
    @IBOutlet weak var leftBoat: UIImageView!
    
    
    @IBOutlet weak var missLeft1: UIButton!
    @IBOutlet weak var missLeft2: UIButton!
    @IBOutlet weak var missLeft3: UIButton!
    @IBOutlet weak var cannLeft1: UIButton!
    @IBOutlet weak var cannLeft2: UIButton!
    @IBOutlet weak var cannLeft3: UIButton!
    
    @IBOutlet weak var missRight1: UIButton!
    @IBOutlet weak var missRight2: UIButton!
    @IBOutlet weak var missRight3: UIButton!
    @IBOutlet weak var cannRight1: UIButton!
    @IBOutlet weak var cannRight2: UIButton!
    @IBOutlet weak var cannRight3: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Init();
    }
    
    func Init() {
        missionaries.append(missLeft1)
        missionaries.append(missLeft2)
        missionaries.append(missLeft3)
        missionaries.append(missRight1)
        missionaries.append(missRight2)
        missionaries.append(missRight3)
        
        cannibals.append(cannLeft1)
        cannibals.append(cannLeft2)
        cannibals.append(cannLeft3)        
        cannibals.append(cannRight1)
        cannibals.append(cannRight2)
        cannibals.append(cannRight3)
        
        Reset()
    }
    
    func Reset() {
        numOfCrossings = 0;
        crossingLabel.text = "\(numOfCrossings) Crossings"
        selectedButtons.removeAll()
        leftBoat.isHidden = false
        rightBoat.isHidden = true
        
        numOfMissionOnLeft = 3
        numOfCannOnLeft = 3
           
        for index in 0...5 {
            missionaries[index].isHighlighted = false
            missionaries[index].backgroundColor = UIColor.white
            cannibals[index].isHighlighted = false
            cannibals[index].backgroundColor = UIColor.white
            
            if (index >= 3) {
                missionaries[index].isHidden = true
                cannibals[index].isHidden = true
                missionaries[index].isEnabled = false
                cannibals[index].isEnabled = false
            } else {
                missionaries[index].isHidden = false
                cannibals[index].isHidden = false
                missionaries[index].isEnabled = true
                cannibals[index].isEnabled = true
            }
        }
    }

    @IBAction func clickedPersonButton(_ sender: UIButton) {
        if (selectedButtons.contains(sender)) {
            selectedButtons = selectedButtons.filter{ $0 != sender}
            sender.isHighlighted = false
            sender.backgroundColor = UIColor.white
            return;
        }
        if (selectedButtons.count < 2) {
            selectedButtons.append(sender);
            sender.isHighlighted = true
            sender.backgroundColor = UIColor.red
        }
    }
    
    @IBAction func ResetAction(_ sender: Any) {
        Reset()
    }
    
    @IBAction func Cross(_ sender: UIButton) {
        if(selectedButtons.count == 0) { return; }
        
        numOfCrossings += 1;
        crossingLabel.text = "\(numOfCrossings) Crossings"
        
        for button in selectedButtons {
            let isRight = button.tag > 2 ? true : false
            let isCannibal = cannibals.contains(button) ? true : false
            
            button.isHighlighted = false;
            button.backgroundColor = UIColor.white
            button.isHidden = true;
            SwapButtons(button, isCannibal)
            if (isCannibal) {
                numOfCannOnLeft += isRight ? 1 : -1
            } else {
               numOfMissionOnLeft += isRight ? 1 : -1
            }
        }
        selectedButtons.removeAll()
        ToggleButtons()
        CheckWinLoseCondition()
    }
    
    func SwapButtons(_ button: UIButton, _ isCannibal: Bool) {
        if (isCannibal) {
            cannibals[(button.tag + 3) % 6].isHidden = false
        } else {
            missionaries[(button.tag + 3) % 6].isHidden = false
        }
    }
    
    func ToggleButtons() {
        for index in 0...5 {
            missionaries[index].isEnabled = !missionaries[index].isEnabled
            cannibals[index].isEnabled = !cannibals[index].isEnabled
        }
        leftBoat.isHidden = !leftBoat.isHidden
        rightBoat.isHidden = !rightBoat.isHidden
    }
    
    func AlertUser(title: String, message: String) {
        let uiac: UIAlertController = UIAlertController.init(title:title, message: message, preferredStyle: .alert)
                   uiac.addAction(UIAlertAction(title: "Reset", style: .cancel, handler: ResetAction))
                   self.present(uiac, animated: true, completion: nil)
    }
    
    func CheckWinLoseCondition() {
        if(numOfMissionOnLeft == 0 && numOfCannOnLeft == 0) {
           AlertUser(title: "You Won!", message: "Congrats")
        }
        
        if(numOfMissionOnLeft % 3 == 0) { return }
        
        if(numOfMissionOnLeft < numOfCannOnLeft || 3 - numOfMissionOnLeft < 3 - numOfCannOnLeft) {
            AlertUser(title: "You Lost!", message: "Consider Uninstalling")
        }
    }
}

