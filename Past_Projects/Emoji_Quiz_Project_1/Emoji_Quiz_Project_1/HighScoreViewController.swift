//
//  HighScoreViewController.swift
//  Emoji_Quiz_Project_1
//
//  Created by Botaish, Mark on 2/7/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class HighScoreViewController: UIViewController {
    
    @IBOutlet weak var testing: UILabel!
    @IBOutlet weak var goldLabel: UILabel!
    @IBOutlet weak var silverLabel: UILabel!
    @IBOutlet weak var bronzeLabel: UILabel!
    @IBOutlet weak var highScoreLabel: UILabel!
    
    var scores : [UILabel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scores.append(goldLabel)
        scores.append(silverLabel)
        scores.append(bronzeLabel)
        SetLocatization();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let dict = (UserDefaults.standard.dictionary(forKey: "HIGH_SCORES")){
            let castedDict = dict as! [String: Int]
            let sortedKeys = castedDict.sorted { $0.1 > $1.1 }
            for i in 0..<sortedKeys.count{
                let player = sortedKeys[i]
                scores[i].text = "\(player.key) - \(player.value)"
            }
        } else { print("Could not find high scores!") }
    }   

    func SetLocatization(){
        highScoreLabel.text = NSLocalizedString("High_Score_Text", comment: "")
        
    }
}
