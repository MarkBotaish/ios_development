//
//  AboutViewController.swift
//  Emoji_Quiz_Project_1
//
//  Created by Botaish, Mark on 2/13/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//


import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var createdText: UILabel!
    @IBOutlet weak var classText: UILabel!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var projectText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetLocatization()
        // Do any additional setup after loading the view.
    }

    
    func SetLocatization(){
        createdText.text = "\(NSLocalizedString("Created_Text", comment: "")): Mark Botaish"
        classText.text = "\(NSLocalizedString("Class_Test", comment: "")): iOS Development"
        dateText.text = "\(NSLocalizedString("Date_Text", comment: "")): 2/13/20"
        projectText.text = "\(NSLocalizedString("Project_Name_Text", comment: "")): Emoji Quiz"

        
    }
}
