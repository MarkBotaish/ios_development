//
//  ViewController.swift
//  Emoji_Quiz_Project_1
//
//  Created by Botaish, Mark on 2/6/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var quizTypeLabel: UILabel!
    @IBOutlet weak var genreSegment: UISegmentedControl!
    @IBOutlet weak var startButton: UIButton!
    
    var genreFlag = 0;
    var numOfQuestions = 1;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetLocatization()
        // Do any additional setup after loading the view.
    }

    @IBAction func ChangeQuestions(_ sender: UIStepper, forEvent event: UIEvent) {
        questionLabel.text = "\(NSLocalizedString("Num_Question_Text", comment: "")): \(Int(sender.value))"
        numOfQuestions =  Int(sender.value)
    }
    
    @IBAction func ChangeGenre(_ sender: UISegmentedControl, forEvent event: UIEvent) {
        genreFlag = (genreFlag + 1) % 2
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! EmojiQuizController
        controller.genreFlag = genreFlag
        controller.numOfQuestions = numOfQuestions
    }
    
    func SetLocatization(){
        titleLabel.text = NSLocalizedString("Title_Text", comment: "")
        questionLabel.text = "\(NSLocalizedString("Num_Question_Text", comment: "")): \(numOfQuestions)"
        quizTypeLabel.text = NSLocalizedString("Quiz_Type_Text", comment: "")
        genreSegment.setTitle(NSLocalizedString("Movie_Text", comment: ""), forSegmentAt: 0)
        genreSegment.setTitle(NSLocalizedString("TV_Text", comment: ""), forSegmentAt: 1)
        startButton.setTitle(NSLocalizedString("Start_Test", comment: ""), for: .normal)
        
    }
    @IBAction func unwindToMainMenu(segue: UIStoryboardSegue) {}
}

