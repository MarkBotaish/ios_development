//
//  EmojiQuizController.swift
//  Emoji_Quiz_Project_1
//
//  Created by Botaish, Mark on 2/7/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit
import AVFoundation

class EmojiQuizController: UIViewController {
    
    @IBOutlet weak var guessLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var livesLabel: UILabel!
    
    var genreFlag = 0
    var numOfQuestions = 0
    var score = 0;
    
    var correctWord = ""
    var guess = ""
    var lives = 3
    
    var guessPool : [String] = []
    var questions: [String : String] = [:]
    var highScores: [String : Int] = [:]
    var keys: [String] = []
    var buttons: [UIButton] = []
    
    var files = ["MovieQuestions", "TVQuestions"]
    var secretWord : String = ""
    
    var currentTimer = 0
    let totalTime = 30
    var timer : Timer = Timer()
    
    var livesText : String = ""
    var timerText : String = ""
    var winText : String = ""
    var loseText: String = ""

    var session: AVAudioSession!
    var correctSound:AVAudioPlayer!
    var wrongSound:AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetLocatization()
        livesLabel.text = "\(livesText): \(lives)"
        timerLabel.text = "\(timerText): \(currentTimer)"
        if let dict = UserDefaults.standard.dictionary(forKey: "HIGH_SCORES"){
            highScores = dict as! [String : Int]
        } else { print("Could not find high scores!") }
        LoadDictionary(index: genreFlag)
        AudioSetupCorrect()
        AudioSetupWrong()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        score = 0;
        lives = 3
        livesLabel.text = "\(livesText): \(lives)"
        timerLabel.text = "\(timerText): \(currentTimer)"
        keys = Array(questions.keys)
        for _ in 0..<(5 - numOfQuestions) {keys.removeLast()}
        TestNextQuestion()
    }
    
    func StartTimer(){
        currentTimer = totalTime;
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
    }
    
    @objc func fire(){
        currentTimer -= 1
        timerLabel.text = "\(timerText): \(currentTimer)"
        if(currentTimer <= 0) {
            Die()
            if(lives > 0) { TestNextQuestion() }
        }
    }
    
    func Die(){
        lives -= 1
        livesLabel.text = "\(livesText): \(lives)"
        wrongSound?.play()
        if(lives == 0){
            currentTimer = 0
            timer.invalidate()
            DisplayPopUp(NSLocalizedString("Lose_Text", comment: ""))
        }
    }
    
    @IBAction func Guess(_ sender: UIButton) {
        buttons.append(sender)
        sender.isEnabled = false;
        TestLetter(sender.titleLabel!.text!)
    }
    
    func TestNextQuestion() {
        //If there are more questions to play play them.
        if(keys.count > 0){
            StartTimer()
            questionLabel.alpha = 0;
            UIView.animate(withDuration: 2.0) { self.questionLabel.alpha = 1 }
            
            let key = String(GetRandomKeyAndRemove())
            guessPool.removeAll()
            
            correctWord = questions[key]!.lowercased()
            questionLabel.text = key
            updateSeretWord()
            
            buttons.forEach {$0.isEnabled = true}
            buttons.removeAll()
        } else { //User wins if they completed all questions
            timer.invalidate()
            DisplayPopUp(NSLocalizedString("Win_Text", comment: ""))
        }
    }
    
    func DisplayPopUp(_ status: String){
        UserDefaults.standard.setValue(lives, forKey: NSLocalizedString("Score_Text", comment: ""))
        let uiac: UIAlertController = UIAlertController.init(title:status, message: "\(NSLocalizedString("Score_Text", comment: "")): \(score)", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: NSLocalizedString("Save_Text", comment: ""), style: .cancel, handler: { alert -> Void in
            self.SaveScore(s: (uiac.textFields?[0].text)!)
        })
        
        uiac.addTextField{ textField in textField.placeholder = NSLocalizedString("Username_Text", comment: "") }
        uiac.addAction(saveAction)
        self.present(uiac, animated: true, completion: nil)
    }
    
    func SaveScore(s : String){
        var newName : String = s;
        if(newName.count == 0) { newName = "No Name" }
        
        var sortedKeys = highScores.sorted { $0.1 > $1.1 }
        var counter = 1;
        //Makes sure the name is not already on the leaderboard. If so add a (num) after name
        if(highScores[newName] != nil) {
           while(highScores["\(newName) \(counter)"] != nil){
               counter+=1;
           }
           newName = "\(newName) (\(counter))"
        }
        //Place score in the correct position
        for index in 0...2 {
           if index >= sortedKeys.count{
               sortedKeys.append((key: newName, value: score))
               break;
           }
           if(score > sortedKeys[index].value ) {
               sortedKeys.insert((key: newName, value: score), at: index)
               break
           }
        }
        //Clear Dictionary and replace it with the new score
        highScores.removeAll()
        for index in 0...2 {
            if index < sortedKeys.count {
                highScores[sortedKeys[index].key] = sortedKeys[index].value
            } else { break; }
        }
        UserDefaults.standard.set(highScores, forKey: "HIGH_SCORES")
        self.performSegue(withIdentifier: "unwindToMainVC", sender: self) //Goes back to the main meu
    }
    
    
    func updateSeretWord() {
  
        secretWord = String(correctWord.compactMap {
            if ($0 == " ") { return $0 }
            if guessPool.contains(String($0)) { return $0 }
            return "-"
        })
        
        guessLabel.text = secretWord.uppercased()
        
        if secretWord == correctWord {
            score += 1
            TestNextQuestion()
            correctSound?.play()
        }
    }
    
    func TestLetter(_ guess: String) {
        
        let firstLetterStr = String(guess).lowercased()
        if !correctWord.contains(firstLetterStr) {
            Die();
        }
        guessPool.append(firstLetterStr)
        updateSeretWord()
    }
    
    func LoadDictionary(index : Int) {
        
        if let path = Bundle.main.path(forResource: files[index], ofType: "txt") {
            do{
                let data = try String(contentsOfFile: path, encoding: .utf8)
                var myStrings = data.components(separatedBy: .newlines)
                myStrings.removeLast()
                
                for lines in myStrings{
                    let line = lines.split(separator: "|")
                    questions[String(line[1])] = String(line[0])
                }
            } catch {
                print("Could not read file \(path)")
            }
            
        } else {
            print("Coud not find file \(files[index])")
        }
        
    }
    
    func GetRandomKeyAndRemove() -> String{
        let index = Int(arc4random_uniform(UInt32(keys.count)))
        let key = keys[index]
        keys.remove(at: index)
        return key
    }
    
    func AudioSetupCorrect(){
        if let url = Bundle.main.url(forResource: "Correct", withExtension: "aiff") {
            do {
                let session = AVAudioSession.sharedInstance()
                try! session.setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
                correctSound = try AVAudioPlayer(contentsOf: url)
            } catch let e {
                print(e.localizedDescription)
            }
        } else {
            print("Could not find sound file.")

        }
    }
    
    func AudioSetupWrong(){
        if let url = Bundle.main.url(forResource: "Wrong", withExtension: "aiff") {
            do {
                let session = AVAudioSession.sharedInstance()
                try! session.setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
                wrongSound = try AVAudioPlayer(contentsOf: url)
            } catch let e {
                print(e.localizedDescription)
            }
        } else {
            print("Could not find sound file.")

        }
    }
    
    
    
    func SetLocatization(){
        livesText = NSLocalizedString("Lives_Text", comment: "")
        timerText = NSLocalizedString("Timer_Text", comment: "")
        winText = NSLocalizedString("Win_Text", comment: "")
        loseText = NSLocalizedString("Lose_Text", comment: "")
    }
    
    
}
