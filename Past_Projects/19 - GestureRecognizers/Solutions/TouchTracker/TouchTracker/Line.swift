//
//  Copyright © 2015 Big Nerd Ranch
//

import Foundation
import CoreGraphics
import UIKit

struct Line {
    
    var begin = CGPoint.zero
    var end = CGPoint.zero
    var addedThickness = CGFloat.zero
    var color = UIColor.black
}
