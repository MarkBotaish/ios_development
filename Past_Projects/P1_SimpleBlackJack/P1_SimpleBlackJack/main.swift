//
//  main.swift
//  SimpleBlackjack_PS1
//
//  Created by Botaish, Mark on 1/20/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import Foundation


func calculateScore(x: [String]) -> Int{
    var score: Int = 0
    var hasAce = false
    
    for card in x{
        let cardValue: Int! = Int(card)
        
        if cardValue != nil{
            score += cardValue
        }else{
            if String(card) == "A"{
                hasAce = true
            }else{
                score += 10
            }
        }
    }
    
    if hasAce{
        if (score + 11) > 21 {
            score += 1
        }else{
            score += 11
        }
    }
    
    return score
}

func printStatus(playerCards: [String], dealerCard: [String]){
    print("")
    print("Player's total is  \(calculateScore(x: playerCards))")
    for card in playerCards{
        print(card, terminator: ", ")
    }
    print("")
    
    print("Dealer's total is \(calculateScore(x: dealerCard))")
    for card in dealerCard{
        print(card, terminator: ", ")
    }
    print("")
}

//-------------------

var deck: Array<String> = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]

var playerCards: [String] = []
var dealerCards: [String] = []

deck.shuffle()

print("Dealer draws first cards.")
dealerCards.append(deck.popLast() ?? "2")

print("Player receives two cards.")
playerCards.append(deck.popLast() ?? "2")
playerCards.append(deck.popLast() ?? "2")
printStatus(playerCards: playerCards, dealerCard: dealerCards)

while(true){
    print("Do you want to (H)it, (S)stay, or (Quit)")
    let selection = readLine()?.uppercased()
    
    if selection == "H"{
        playerCards.append(deck.popLast() ?? "2")
        printStatus(playerCards: playerCards, dealerCard: dealerCards)
        
        if(calculateScore(x: playerCards) > 21){
            print("You busted! You lose!")
            exit(0)
        }
    }else if selection == "S"{
        break
    }else if selection == "Q"{
        exit(0)
    }
}

print("Dealer draws rest of cards.")

while(calculateScore(x: dealerCards) < 17){
    dealerCards.append(deck.popLast() ?? "2")
}

printStatus(playerCards: playerCards, dealerCard: dealerCards)

if(calculateScore(x: dealerCards) > 21){
    print("Dealer busts! You win")
}else if(calculateScore(x: dealerCards) > calculateScore(x: playerCards)){
    print("Dealer Wins!")
}else if calculateScore(x: dealerCards) < calculateScore(x: playerCards){
    print("You Win!")
}else{
    print("It's a tie!")
}
