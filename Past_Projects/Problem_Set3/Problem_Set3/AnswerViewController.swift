//
//  AnswerViewController.swift
//  Problem_Set3
//
//  Created by Botaish, Mark on 2/18/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class AnswerViewController: UIViewController {
    
    @IBOutlet weak var answerLabel: UILabel!
    
    var answer = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        answerLabel.text = answer;
    }

}
