//
//  FlashCardTableViewController.swift
//  Problem_Set3
//
//  Created by Botaish, Mark on 2/17/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class FlashCardTableViewController: UITableViewController {

    var keys: [String] = []
    var values: [String] = []
    var currentSelectedWord = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = Bundle.main.path(forResource: "Words", ofType: "plist"){
            let dict = (NSDictionary(contentsOfFile: path) as? Dictionary<String, String>)!
            keys = Array(dict.keys)
            values = Array(dict.values)
        }else{
            print("Could not load file!")
        }
       
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return keys.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FlashCard", for: indexPath)
        if let card: FlashCardCell = cell as? FlashCardCell{
            card.theLabel.text = keys[indexPath.row]
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelectedWord =  indexPath.row
        performSegue(withIdentifier: "AnswerSegue", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AnswerSegue"{
              if let vc = segue.destination as? AnswerViewController{
                vc.answer = values[currentSelectedWord]
              }
        }
    }
    
    @IBAction func SwapLang(_ sender: Any) {
        swap(&keys, &values)
        self.tableView.reloadData()
    }
    
}
