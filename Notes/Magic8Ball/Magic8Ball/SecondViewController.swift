//
//  SecondViewController.swift
//  Magic8Ball
//
//  Created by Botaish, Mark on 1/22/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Save to User Defaults file. One per app
        UserDefaults.standard.set(textField.text, forKey: "name")
        UserDefaults.standard.synchronize()
        
        //The responder currently handling the input field is now done (closes keyboard)
        textField.resignFirstResponder()
        
        print("textFieldShoudReturn called")
        
        return true
    }

}

