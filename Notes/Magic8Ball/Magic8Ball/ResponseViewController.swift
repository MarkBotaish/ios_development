//
//  ResponseViewController.swift
//  Magic8Ball
//
//  Created by Botaish, Mark on 1/22/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class ResponseViewController: UIViewController {

    @IBOutlet weak var responseLabel: UILabel!
    let responses = ["Yes", "Maybe", "No"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let randomNum = Int.random(in: 0...2)
        if let name = UserDefaults.standard.string(forKey: "name"){
            responseLabel.text = responses[randomNum] + ", \(name)"
        }else{
            responseLabel.text = responses[randomNum]
        }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
