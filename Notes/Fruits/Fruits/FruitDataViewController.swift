//
//  FruitDataViewController.swift
//  Fruits
//
//  Created by Botaish, Mark on 2/14/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class FruitDataViewController: UIViewController {

    @IBOutlet weak var fruitLabel: UILabel!
    var fruitName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fruitLabel.text = fruitName
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
