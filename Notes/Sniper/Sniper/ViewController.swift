//
//  ViewController.swift
//  Sniper
//
//  Created by Botaish, Mark on 2/5/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var arrowCenterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var targetCenterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var arrow: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        //arrowCenterConstraint.constant = 50
        //Randomly move target image along x-axis
        let halfScreenWidth = UIScreen.screens[0].bounds.width / 2
        
        let targetMove: CGFloat = CGFloat.random(in: -halfScreenWidth...halfScreenWidth)
        
        targetCenterConstraint.constant = targetMove
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let halfScreenWidth = UIScreen.screens[0].bounds.width / 2
        let screenHeight = UIScreen.screens[0].bounds.height
        let touch = touches.first!
        let touchLocation = (touch.location(in: self.view))
        let oposite = halfScreenWidth - touchLocation.x
        let adjacent = arrow.center.y - touchLocation.y
        let angle = atan2(adjacent, oposite)
        arrow.transform = CGAffineTransform(rotationAngle: angle - CGFloat.pi / 2.0)
        
    }


}

