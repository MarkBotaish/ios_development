//: Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"

var i: Int = 5
var j: Int = 6
var k = i + j

infix operator ⚔️: AdditionPrecedence
struct Point2D {
    let x: Int
    let y: Int
    

    
    static func * (lhs: Point2D, rhs: Int) -> Point2D {
        let newX = lhs.x * rhs
        let newY = lhs.y * rhs
        return Point2D(x: newX, y: newY)
    }
    
    static func * (lhs: Int, rhs: Point2D) -> Point2D {
        let newX = rhs.x * lhs
        let newY = rhs.y * lhs
        return Point2D(x: newX, y: newY)
    }
    
    static func ⚔️ (lhs: Point2D, rhs: Point2D) -> Point2D {
        let newX = lhs.x + rhs.x
        let newY = lhs.y + rhs.y
        return Point2D(x: newX, y: newY)
    }
}

var p1: Point2D = Point2D(x: 4, y: 4)
var p2: Point2D = Point2D(x: 2, y: 1)

var p3: Point2D = p1 ⚔️ p2 // 6, 5
var p4: Point2D = 2 * p3



struct Person{
    let name: String
}

func  +(lhs: Person, rhs: Person)-> Person{
    return Person(name: lhs.name + rhs.name)
}

let pa : Person = Person(name: "John")
let pb : Person = Person(name: "Mary")
let pc = pa + pb


