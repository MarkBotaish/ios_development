//: *CSI 318, David Kopec*
/*:
 ## Comments
 Comments in Swift are similar to comments in C-like languages.
 // is for a one line comment
 */
// here's a one line comment
let x = 0 // this could also be on the end of a line
/*
 here is a multi-line comment
 good for putting your license at the top of your files
 */
//: [Previous](@previous) Page 2 of 10 [Next](@next)
