//: *CSI 318, David Kopec*
/*:
 # Welcome to Swift!
 You're inside a Playground — an interactive Swift execution environment. Today, we're going to go over the basics of the language as suitable for someone with significant C++ experience. Swift is a fairly large language (but not as large as C++!) and we won't be able to cover everything. In fact, we will leave most of the language for incremental updates throughout the semester as we learn iOS app development.
 
 Swift has a relatively small standard library. You will notice that everything we do today does not require any imports. When you do iOS development you have access to many libraries including fundamental helper classes in Foundation. But the Swift standard library, which we will be exploring today, will provide all of your basic types.
 
 >This Playground has multiple pages. You can go to the next page with the link at the bottom or by navigating in Xcode through the left pane.
 */
//: Page 1 of 10 [Next](@next)


