//: *CSI 318, David Kopec*
/*:
 ## Custom Types
 Swift has three different building blocks for defining your own types: enum, struct, and class. Enums and structs are value-types which means when you copy them you get duplicates — two things that are now separate but initially the same. Classes are reference-types. When you copy them you get two references to the same instance.
 
 Swift enums and structs are more powerful than enums and structs in most other languages. We'll briefly touch on protocols too — much more about that to come in future weeks.
 */
enum InnerPlanet {
    case Mercury
    case Venus
    case Earth
    case Mars
}
let x = InnerPlanet.Mercury
//: enums can have functions and extend other types
enum Weekday: Int {
    case Sunday = 0
    case Monday = 1
    case Tuesday = 2
    case Wednesday = 3
    case Thursday = 4
    case Friday = 5
    case Saturday = 6
    
    func previousDay() -> Weekday {
        let previous = self.rawValue - 1
        if let pd = Weekday(rawValue: previous) {
            return pd
        }
        return .Saturday
    }
}
let manic: Weekday = .Monday
let funday = manic.previousDay()
//: Structs are good for immutable data modeling, but they do not have inheritance beyond adopting protocols. You must mark their methods with `mutating` if they will make changes to the contents of the struct.
struct Point {
    var x: Int
    var y: Int
    init(x: Int, y: Int) { // actually created for you for structs if you don't specify an initializer
        self.x = x
        self.y = y
    }
    mutating func move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }
    func distanceTo(p2: Point) -> Float {
        let dx = Float(p2.x - x)
        let dy = Float(p2.y - y)
        return ((dx * dx) + (dy * dy)).squareRoot()
    }
}
var p1 = Point(x: 2, y: 4)
p1.move(dx: 2, dy: 2)
let p2 = Point(x: 10, y: 11)
let distance = p1.distanceTo(p2: p2)
//: `self` is like `this`. You can use public, internal, private, fileprivate, and open access modifiers. These specify where a property, method, or type can be seen and used.
open class Animal {
    func eat() {}
    func sleep() {}
}
public class Dog: Animal {
    func morning() {
        eat()
        sleep()
    }
}
private class SecretBreed: Dog {
    override func morning() {
        sleep()
    }
}
//: We will talk more about protocols another week... but they are basically abstract data types that other types can adopt. There is only single-inheritance with Swift classes, but a single Swift enum, struct, or class can adopt multiple protocols.
protocol Buyer {
    func buy()
}
protocol Seller {
    func sell()
}
struct Trader: Buyer, Seller {
    func sell() {
        print("I sold something.")
    }
    func buy() {
        print("I bought something.")
    }
}
class Shopper: Buyer {
    func buy() {
        print("Fighting my Amazon addiction!")
    }
}
//: [Previous](@previous) Page 9 of 10 [Next](@next)
