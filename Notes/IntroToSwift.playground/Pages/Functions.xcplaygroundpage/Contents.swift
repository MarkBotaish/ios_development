//: *CSI 318, David Kopec*
/*:
 ## Functions
 Swift has first-class support for functions including inner functions, closures, capture lists, generics in functions and much more. Functions can be passed around like other types as well as returned by functions. There are many powerful features, but today we're just going to cover the basics.
 */
func shout(at: String) {
    print("Hey \(at)!")
}
shout(at: "Michael")
shout(at: "Sarah")
//: -> is used for specifying a return type
func double(x: Int) -> Int {
    return x * 2
}
let y = double(x: 4) // 8
//: You can change what a parameter is known as internally to a function.
func triple(x num: Int) -> Int {
    return num * 3
}
//: But that does not change how it's called.
let z = triple(x: 2) // 6
//: You can even specify that no label is required by the caller.
func quadruple(_ num: Int) -> Int {
    return num * 4
}
let a = quadruple(1) // 4
//: Inner functions are sometimes useful, but only use them if they reduce instead of increase confusion.
func multiplyBy4(x: Int) -> Int {
    func multiplyBy2(x: Int) -> Int {
        return x * 2
    }
    return multiplyBy2(x: x) + multiplyBy2(x: x)
}
let b = multiplyBy4(x: 10)
//: Closures are similar in concept to anonymous functions, lambdas, and such in other languages. They are basically just little functions with slightly different syntax that can "capture" some of the variables around them. They will become more useful as we get further into UIKit.
let constructString = { () -> String in
    var s = "Hello"
    s.append("!")
    s.append("!")
    return s.uppercased()
}
let constructedString: String = constructString()
let doubler = {x in
    return x * 2
}
let four = doubler(2)
//: Swift can be written in a functional-style and the standard library has built-in support for functional programming mainstays like map(), filter(), and reduce().
let smallWords = ["hey", "wow", "no", "ah", "ay"]
let twoLetterShouts = smallWords.map{ $0.uppercased() }.filter { $0.count == 2 }
twoLetterShouts
//: [Previous](@previous) Page 7 of 10 [Next](@next)



