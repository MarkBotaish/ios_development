//: *CSI 318, David Kopec*
/*:
 ## Collections
 Swift's fundamental built-in collection types include (but are not limited to) Array, Dictionary (map/associative array in other programming languages), and Set. As of Swift 4, a String is also a collection.
 */
//: Arrays
let a: [Int] = [1, 2, 3, 4]
let b: Array<Int> = [1, 2, 3, 4] // equivalent to above
let c = [1, 2, 3, 4] // equivalent to above
let d: [Int] = [] // empty Int array
let e: Array<Int> = Array<Int>() // also empty Int array
var f: Array<String> = ["apple", "banana"]
//: All of the standard methods you'd expect exist on Array as well as the standard subscripting with the first index being zero.
f.append("cantaloupe")
f.popLast() // remove and return cantaloupe
let g = f[1] // banana
//: Multidimensional arrays in Swift can be a little confusing coming from c-style languages.
let h: [[Int]] = [[1, 2, 3], [2, 3, 4]]
let i = h[0][2] // 3
//: Subscripting is surprisingly unsafe for a safe language like Swift — if you go off the end of the array, you will crash. You can use for-in loops with Arrays.
for fruit in f {
    print(fruit)
}
//: Dictionaries
let ages: [String : UInt] = ["Sarah": 30, "Mary": 35, "Mark": 60, "Vanessa": 30]
let sarahAge = ages["Sarah"]
//: Dictionaries can also be used with for-in loops.
for (name, age) in ages {
    print("\(name) is \(age).")
}
//: What you're actually getting above with `(name, age)` is a tuple... a tuple is not actually a collection, but it's worth mentioning here. It's a grouped set of values that works similarly to a Python tuple if you've ever seen that before. It can be more than two values. You won't see tuples in UIKit because they don't exist in Objective-C. They are sometimes useful for returning more than one value from a function.
typealias Point = (x: Int, y: Int) // typealias renames a type, here I'm renaming a tuple type
let p1: Point = (x: 4, y: 23)
p1.0 // first value in a tuple
p1.x // equivalent to above
//: Now we can have functions that return `Point`. Recall that sets are collections that cannot have duplicates. We can use Array-like syntax for defining set literals.
let newEngland: Set<String> = ["Maine", "New Hampshire", "Connecticut", "Rhode Island", "Vermont", "Massachusetts"]
let northernNewEngland: Set<String> = ["Maine", "New Hampshire", "Vermont"]
northernNewEngland.isSubset(of: newEngland)
//: Rememember that sets are a great way to check for duplicates in arrays.
let animals: [String] = ["dog", "dog", "cat"]
let animalsSet: Set<String> = Set(animals)
if animalsSet.count < animals.count {
    print("There are duplicates!")
}
//: [Previous](@previous) Page 6 of 10 [Next](@next)


