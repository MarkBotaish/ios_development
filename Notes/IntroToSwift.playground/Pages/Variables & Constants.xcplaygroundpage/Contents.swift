//: *CSI 318, David Kopec*
/*:
 ## Variables & Constants
 Variables are declared with `var` and constants are declared with `let`. Unlike standard variables, constants cannot be changed from their initial value. It is considered good practice to use constants whenever possible.
 */
var x = 0
x = 1 // legal
//: Notice that we don't use semicolons (;) in Swift. They are optional.
let y = 4
// y = 3 is illegal, because y was declared with let
//: Swift has type inference, so you can declare variables without declaring their types. However, for readability, you'll usually see me declare variables along with their types. Unlike in C-like languages, types in Swift come after the variable name. They come after a colon (:).
let z: Int = 45
let h: Float = 2.39
//: Be careful, because sometimes the type that Swift is inferring may not be what you're thinking. It's a good idea to just put the type in anytime that you're unsure.
let a = 102.87 // Is this a Float or Double? Don't remember the default, better just put it in.
//: [Previous](@previous) Page 3 of 10 [Next](@next)

