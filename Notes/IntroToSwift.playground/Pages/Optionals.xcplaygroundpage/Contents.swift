//: *CSI 318, David Kopec*
/*:
 ## Optionals
 In other languages you're probably used to pointers or references that can sometimes be null. In Swift, we call null "nil," and by default variables can never be nil. An optional is a special type of variable that can be nil. We signify an optional by adding a question mark (?) to the end of a type definition.
 */
var x: Int // can never be nil
x = 5
// x = nil // would be illegal
var y: Int? // can be nil
y = 5
y = nil
//: Making a type an optional type is like wrapping the variable inside a question: "Are you nil?" All optional variables are either nil or a value. Since non-optional types can never be nil (which is nice for safety) we cannot set a non-optional to be the value of an optional by default. However, we can go the other way.
// x = y // illegal
y = x // legal
//: Optionals can be *unwrapped* using an `if let` statement
y = 6
if let z = y {
    print(z) // z will take on the value of y only if y is not nil, otherwise this body will never execute
}
//: You can also force unwrap an optional with `!`. This will fail and crash your program if the optional is nil when you do it, so it is dangerous and should be avoided in general.
let a = y!
//: Implicitly Unwrapped Optionals (IUOs) are optionals that can be used as if they were not wrapped all the time... they are dangerous and should be avoided but for the purposes of bridging with Objective-C you will sometimes see them. You declare an IUO with `!`
var b: Int! = 3
var c: Int
c = b // legal, but unsafe... what if b were nil?!
//: [Previous](@previous) Page 8 of 10 [Next](@next)




