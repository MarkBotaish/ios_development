//: *CSI 318, David Kopec*
/*:
 ## Basic Types
 The Swift standard library includes all of the basic types you would expect from integers to floating point numbers to strings and characters. Here are just some of the types in the standard library.
 */
//: Some numbers:
let n1: Int = 0 // 64 bit on 64 bit platforms
let n2: UInt = 1  // unsigned; can't be negative
let n3: UInt8 = 200 // limited to 8 bits
let n4: Float = 23.3 // single-precision floating point
let n5: Double = 23.3 // double-precision floating point
let n6 = (3 * 3 + (4 - 7)) // standard arithmetic operators are present
//: Swift does not have type coercion, so you can't just multiply an Int by a UInt
// let n7 = n1 * n2 // this is not legal
let n7 = n1 * Int(n2) // you have to first convert the types to be compatible
//: Booleans
let b1: Bool = true
let b2: Bool = false
let b3 = !b1 // not operator
//: Strings and Characters
let s1: String = "Wow!"
let s2: Character = "A" // just one character allowed
//: Strings have the full Unicode character set allowed
let s3: String = "🇺🇸👍🏼🤠👩‍🔬🐠"
//: Values can be interpolated in strings using the \\() syntax where the value to be interpolated goes between the parentheses.
let s4: String = "Computer are all based on the numbers \(n1) and \(n2)."
//: All of the basic types are not "primitives" in the sense of C or Java; they have methods on them, initializers, and properties.
let n8 = n4.rounded()
let n9 = s1.count
//: I encourage you to explore the basic Swift types in the [Swift standard library documentation](https://developer.apple.com/documentation/swift).
let s5: String = "You're doing great!".uppercased()
//: [Previous](@previous) Page 4 of 10 [Next](@next)
