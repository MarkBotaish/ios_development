//: *CSI 318, David Kopec*
/*:
 ## Control Structures
 Most of the control structures you'd expect are in Swift, including if-statements, while-loops, for-loops, for-each loops, and switch statements. C-style for-loops are missing (they were removed in version 2 of the language).
 */
//: If-statements
let temperature = 55
if temperature > 70 { // notice parentheses not reuqired
    print("It's warm!") // your first print function call!
} else if temperature > 32 {
    print("It's mild!")
} else {
    print("It's cold!")
}
//: Loops
var x = 5
while x > 0 {
    print(x)
    x -= 1 // there is no -- or ++ operator... also removed in Swift 2 (or was it Swift 3?)
}
for y in 100..<103 {
    print(y)
}
//: The `100..<103` is a range. It goes from the first number up-to but not including the second. The syntax `100...103` is used for inclusive ranges. Since there are no c-style for-loops in Swift, you will probably use ranges regularly.
//:
//: Switch statements
let z = 2
switch z {
case 0:
    print("zero")
case 2:
    print("two") // no need for break in Swift, it's done automatically, you have to explicitly fallthrough if you want that behavior
case 4:
    print("four")
default:
    print("default") // Switch statements must be exhaustive, although this may change in a future version of Swift
}
//: [Previous](@previous) Page 5 of 10 [Next](@next)

