//: *CSI 318, David Kopec*
/*:
 ## Exercises
 Try to complete the following exercises:
 1. Write a function that finds an arbitrary number in the Fibonacci sequence in Swift.
 2. Write a Shape protocol with properties area & circumference. Write classes Circle, Rectangle, and Square that adopt the Shape protocol. Ensure that Square is a subclass of Rectangle. Write a couple tests to show they work.
 3. Write a function that will perform a binary search on an array of sorted Strings. You may assume the Strings are already sorted.
 4. Write a function that reverses every letter in each word in a sentence, but leaves the words in the same order relative to one another. So for example "Hi there cat." becomes "iH ereht .tac" You will need to almost certainly look through the Swift standard library to complete this task.
 */
//: [Previous](@previous) Page 10 of 10
