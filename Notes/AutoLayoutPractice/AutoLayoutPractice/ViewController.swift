//
//  ViewController.swift
//  AutoLayoutPractice
//
//  Created by Botaish, Mark on 1/29/20.
//  Copyright © 2020 Botaish, Mark. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func goToFinal(_ sender: Any) {
        self.performSegue(withIdentifier: "finalDirect", sender: self)
    }
    
}

