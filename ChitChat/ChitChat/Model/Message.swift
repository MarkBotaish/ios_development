//
//  Message.swift
//  ChitChat
//
//  Created by user166622 on 4/19/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import Foundation

struct Message{
    var id : String = ""
    var client: String = ""
    var date: String = "" //Might need to formart this
    var likes: Int = 0
    var ip: String = ""
    var dislikes: Int = 0
    var location : [String?] = []
    var message: String  = ""
    
    var hasLocalReaction : Bool = false
    

    
}
