//
//  MessageCell.swift
//  ChitChat
//
//  Created by user166622 on 4/19/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit
import Foundation

class MessageCell: UITableViewCell {

    @IBOutlet weak var DislikeButton: UIButton!
    @IBOutlet weak var LikeButton: UIButton!
    @IBOutlet weak var TimeOfMessage: UILabel!
    var index: Int = 0
    var cell: UITableViewCell? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func DislikeMessage(_ sender: Any) {
        UpdateButtons(1,0)
        MessageBoard.shared.ReactToPost(index, false)
        DisableButtons()
    }
    
    @IBAction func LikeMessage(_ sender: Any) {
        UpdateButtons(0,1)
        MessageBoard.shared.ReactToPost(index, true)
        DisableButtons()
    }
    
    func DisableButtons(){
        LikeButton.isEnabled = false
        DislikeButton.isEnabled = false
    }
    
    func EnableButton(){
        LikeButton.isEnabled = true
        DislikeButton.isEnabled = true
    }
    
    func UpdateButtons(_ dislike: Int,_ like : Int){
        if let messageCell = cell as? MessageCell {
            messageCell.cell = cell
            messageCell.LikeButton.setTitle(String(MessageBoard.shared.allMessage[index].likes + like) + " likes", for: .normal)
            messageCell.DislikeButton.setTitle(String(MessageBoard.shared.allMessage[index].dislikes + dislike) + " dislikes", for: .normal)

        }

    }
    
}
