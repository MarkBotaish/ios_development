//
//  MessageBoard.swift
//  ChitChat
//
//  Created by user166622 on 4/19/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import Foundation
import UIKit

protocol MessageBoardDelegate {
    func FinishedLoading()
}

class MessageBoard{
    
    static let shared: MessageBoard = MessageBoard()
    
    private var reactdToPost: [String: Int] = [:]
    
    private var delegate: MessageBoardDelegate? = nil
    
    private let url = "https://www.stepoutnyc.com/chitchat"
    private let id = "mark.botaish@mymail.champlain.edu"
    private let key = "cad381d8-de26-4ea9-8db9-778a971f0286"
    
    private let session: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }()
    
    var allMessage = [Message]()
    
    func SetDelegate(view: MessageBoardTableViewController){
        delegate = view
    }
    
    func LoadSavedPosts(){
         let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("LikedPost.plist")
        if let xml = FileManager.default.contents(atPath: path.path){
           
            reactdToPost = (try? PropertyListSerialization.propertyList(from: xml, options: .mutableContainersAndLeaves, format: nil)) as? [String:Int] ?? [:]
            
        }
    }
    
    func Reload(){
        allMessage.removeAll()
        reactdToPost.removeAll()
        LoadMessages()
    }
    
    func ReloadTable(){
        delegate?.FinishedLoading()
    }
    
    
    //https://learnappmaking.com/plist-property-list-swift-how-to/#writing-data-to-a-plist
    private func SavePost(_ postID: String){
        reactdToPost[postID] = 0
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml

        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("LikedPost.plist")
        do {
            let data = try encoder.encode(reactdToPost)
            try data.write(to: path)
        } catch {
            print(error)
        }
        
    }
    
    //CODE COMES FROM THE BOOK
    func LoadMessages(_ skip: Int = 0, _ max:Int = 20){
        LoadSavedPosts()
        let finalURL = CreateURLForRetrieveMessage(skip, max)
        GetJSONFromURL(finalURL)
    }
    
    func PostMessage(_ msg: String){
        let finalURL = CreateURLForSendMessage(msg)
        PostMessage(finalURL)
    }
    
    func ReactToPost(_ index: Int, _ hasLiked: Bool){
        SavePost(allMessage[index].id)
        let finalURL = CreateURLForReacting(index, hasLiked)
        SendReactionToServer(finalURL)
    }
    
    private func CreateURLForReacting(_ index: Int, _ hasLiked: Bool)-> URL{
        
        let urlAddition = hasLiked ? "/like/\(allMessage[index].id)" : "/dislike/\(allMessage[index].id)"
        var completeURL = URLComponents(string: url + urlAddition)
              
        var QueryItems = [URLQueryItem]()
              
        QueryItems.append(URLQueryItem(name: "client", value: id))
        QueryItems.append(URLQueryItem(name: "key", value: key))
           
        completeURL?.queryItems = QueryItems
           
        return (completeURL?.url!)!
    }

    private func CreateURLForRetrieveMessage(_ skip: Int = 0, _ max:Int = 20)-> URL{
        var completeURL = URLComponents(string: url)
           
        var QueryItems = [URLQueryItem]()
           
        QueryItems.append(URLQueryItem(name: "client", value: id))
        QueryItems.append(URLQueryItem(name: "key", value: key))
        QueryItems.append(URLQueryItem(name: "skip", value: String(skip)))
        QueryItems.append(URLQueryItem(name: "limit", value: String(max)))
        
        completeURL?.queryItems = QueryItems
        
        return (completeURL?.url!)!
    }
    
    private func CreateURLForSendMessage(_ msg: String)-> URL{
        var completeURL = URLComponents(string: url)
           
        var QueryItems = [URLQueryItem]()
           
        QueryItems.append(URLQueryItem(name: "client", value: id))
        QueryItems.append(URLQueryItem(name: "key", value: key))
        QueryItems.append(URLQueryItem(name: "message", value: msg))
        
        completeURL?.queryItems = QueryItems
        
        return (completeURL?.url!)!
    }
    
    private func PostMessage(_ postURL: URL){
        var request = URLRequest(url: postURL)
        request.httpMethod = "POST"
        let task = session.dataTask(with: request) {
                (data, response, error) -> Void in
            if let _ = data {
               print("Success")
               self.Reload()
            } else if let requestError = error {
                print("Error fetching interesting photos: \(requestError)")
            } else {
                print("Unexpected error with the request")
            }
        }
        task.resume()
    }
    
    private func SendReactionToServer(_ requestURL: URL){
           let request = URLRequest(url: requestURL)
           let task = session.dataTask(with: request) {
                   (data, response, error) -> Void in
               if let _ = data {
                  print("Success")
               } else if let requestError = error {
                   print("Error fetching interesting photos: \(requestError)")
               } else {
                   print("Unexpected error with the request")
               }
           }
           task.resume()
       }
    
    private func GetJSONFromURL(_ requestURL: URL){
        let request = URLRequest(url: requestURL)
        let task = session.dataTask(with: request) {
                (data, response, error) -> Void in
            if let jsonData = data {
                self.FillMessages(jsonData)
                 self.ReloadTable()
            } else if let requestError = error {
                print("Error fetching interesting photos: \(requestError)")
            } else {
                print("Unexpected error with the request")
            }
        }
        task.resume()
    }
    
    private func FillMessages(_ data: Data){
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data,
                                                              options: [])
           guard
                let jsonDictionary = jsonObject as? [AnyHashable:Any],
            let messages = jsonDictionary["messages"] as? [[String:Any]] else {
                //let count = jsonDictionary["count"] as? Int,
                //let date = jsonDictionary["date"] as? String else {
                    print("The JSON structure doesn't match our expectations")
                    return
            }
            for message in messages{
                var msg = Message()
                msg.id = message["_id"] as! String
                msg.client = message["client"] as! String
                msg.date = message["date"] as! String
                msg.dislikes = message["dislikes"] as! Int
                msg.ip = message["ip"] as! String
                msg.likes = message["likes"] as! Int
                msg.location = message["loc"] as! [String?]
                msg.message = message["message"] as! String
                msg.hasLocalReaction = reactdToPost[msg.id] != nil
                allMessage.append(msg)

            }
            print("Loaded Completed")
        } catch let error {
            print("Error creating JSON object: \(error)")
        }
    }
    
    
}
