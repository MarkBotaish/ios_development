//
//  MessageBoardTableViewController.swift
//  ChitChat
//
//  Created by user166622 on 4/19/20.
//  Copyright © 2020 MarkBotaish. All rights reserved.
//

import UIKit

class MessageBoardTableViewController: UITableViewController, UITextFieldDelegate, MessageBoardDelegate {

    @IBOutlet weak var InputMessage: UITextField!
    
    var isRefreshing = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MessageBoard.shared.SetDelegate(view: self)
         MessageBoard.shared.LoadMessages()
        
        self.refreshControl?.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        
        self.InputMessage.delegate = self
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func refresh(sender: Any ){
        isRefreshing = true
        MessageBoard.shared.Reload()
    }
    
    func FinishedLoading() {       
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
            self.isRefreshing = false
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        isRefreshing = true
        InputMessage.resignFirstResponder()
        MessageBoard.shared.PostMessage(textField.text!)
        InputMessage.text = ""
        return true;
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return MessageBoard.shared.allMessage.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Mesage", for: indexPath)
        
        if isRefreshing {return cell}

        let currentMessage = MessageBoard.shared.allMessage[indexPath.row]
        
        cell.textLabel?.text = currentMessage.message
        if let messageCell = cell as? MessageCell {
            messageCell.cell = cell
            messageCell.TimeOfMessage.text = currentMessage.date
            messageCell.index = indexPath.row
            messageCell.LikeButton.setTitle("\(currentMessage.likes) Likes", for: .normal)
            messageCell.DislikeButton.setTitle("\(currentMessage.dislikes) Dislikes", for: .normal)
            messageCell.EnableButton()
            if currentMessage.hasLocalReaction {
                messageCell.DisableButtons()
            }
        }

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
